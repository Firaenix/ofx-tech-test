export const CurrencyCodes = [
  'AUD',
  'CAD',
  'CHF',
  'EUR',
  'GBP',
  'HKD',
  'ILS',
  'MXN',
  'TWD',
  'USD',
  'ZAR',
  'ZMK',
  'ZRN',
  'ZWD'
];
