import { inject } from 'mobx-react';
import React from 'react';
import { RouteChildrenProps } from 'react-router';

import { Button } from '../../components/Button';
import { OFXSpotRateResponse } from '../../models/OFXSpotRateResponse';
import { Stores } from '../../stores/RootStore';

type RouterState = {
  toCurrency: string;
  fromCurrency: string;
  amount: number;
};

interface Props extends RouteChildrenProps<{}, RouterState> {
  apiResults?: OFXSpotRateResponse;
}

@inject(
  (stores: Stores): Partial<Props> => ({
    apiResults: stores.store.apiResponse
  })
)
export class SpotResult extends React.Component<Props> {
  private renderFromAmount(apiResults: OFXSpotRateResponse) {
    const fromCurrency = this.props.location.state.fromCurrency;
    const { CustomerAmount, CustomerRateInverse } = apiResults;

    const amt = CustomerAmount * CustomerRateInverse;
    return `${fromCurrency} $${amt.toFixed(2)}`;
  }

  private renderToAmount(apiResults: OFXSpotRateResponse) {
    const toCurrency = this.props.location.state.toCurrency;
    const { CustomerAmount } = apiResults;

    const amt = CustomerAmount;
    return `${toCurrency} $${amt.toFixed(2)}`;
  }

  public render() {
    const { apiResults } = this.props;

    // Make sure to send the users to the home page if we dont have results
    if (!apiResults) {
      this.props.history.push('/');
      return null;
    }

    const { CustomerRate } = apiResults;

    return (
      <div>
        <div className="ofx-title">OFX Customer Rate</div>
        <div className="ofx-customer-rate">{CustomerRate}</div>

        <br />

        <div className="ofx-title">From</div>
        <div className="ofx-customer-rate">{this.renderFromAmount(apiResults)}</div>

        <br />

        <div className="ofx-title">To</div>
        <div className="ofx-customer-rate">{this.renderToAmount(apiResults)}</div>

        <br />
        <br />

        <Button onClick={() => this.props.history.push('/')}>START A NEW QUOTE</Button>
      </div>
    );
  }
}
