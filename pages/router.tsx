import { Col, Row } from 'antd';
import { Provider } from 'mobx-react';
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import { PageHeader } from '../components/PageHeader';
import { RootStore } from '../stores/RootStore';
import { Home } from './Home';
import { SpotResult } from './SpotResult';

export const Routes = () => {
  return (
    <Provider store={RootStore}>
      <BrowserRouter>
        <Row>
          <Col xs={24}>
            <PageHeader />
          </Col>
        </Row>
        <Row>
          <Col xs={1} lg={6} xxl={8} />
          <Col xs={22} lg={12} xxl={8}>
            <Route exact path="/" component={Home} />
            <Route exact path="/spotresults" component={SpotResult} />
          </Col>
          <Col xs={1} lg={6} xxl={8} />
        </Row>
      </BrowserRouter>
    </Provider>
  );
};
