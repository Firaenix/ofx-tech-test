// Allow Async functions to become generator functions
import 'regenerator-runtime/runtime';

import React from 'react';
import ReactDOM from 'react-dom';

import { Routes } from './router';

ReactDOM.render(<Routes />, document.getElementById('app'));
