import { Col, Form, Icon, message, notification, Row, Spin } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { AxiosError } from 'axios';
import { inject } from 'mobx-react';
import React from 'react';
import { RouteChildrenProps } from 'react-router';

import { Button } from '../../components/Button';
import { InputField } from '../../components/Form/InputField';
import { SelectField } from '../../components/Form/SelectField';
import { CurrencyCodes } from '../../models/CurrencyCodes';
import { OFXSpotRateResponse } from '../../models/OFXSpotRateResponse';
import { Stores } from '../../stores/RootStore';

interface Props extends RouteChildrenProps, FormComponentProps {
  apiData?: OFXSpotRateResponse;
  isLoadingData?: boolean;

  fetchAPIData: (to: string, from: string, amount: number) => void;
}

@inject(
  (stores: Stores): Partial<Props> => ({
    apiData: stores.store.apiResponse,
    isLoadingData: stores.store.isFetchingAPI,
    fetchAPIData: stores.store.fetchAPIResponse
  })
)
@(Form.create({ name: 'quick-quote' }) as any) // Little hack to stop typescript nagging, this does work as a decorator
export class Home extends React.Component<Props> {
  private renderSpinnerText() {
    return 'Please wait, we are fetching the current spot prices...';
  }

  private renderLoadingIndicator() {
    return <Icon type="loading" style={{ fontSize: 24 }} spin />;
  }

  private onSubmit = (e: React.FormEvent<HTMLElement>) => {
    // Stop form field from redirecting on submit click
    e.preventDefault();

    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (err) {
        console.log('Failed validation', err);
        return;
      }

      const toCurrency = values['To Currency'];
      const fromCurrency = values['From Currency'];
      const amount = values['Amount'];

      try {
        await this.props.fetchAPIData(toCurrency, fromCurrency, amount);

        // Redirect to results page - request success
        this.props.history.push({
          pathname: '/spotresults',
          state: { toCurrency, fromCurrency, amount }
        });
      } catch (error) {
        const axiosError: AxiosError<OFXSpotRateResponse> = error;

        if (!axiosError.response) {
          notification.error({
            message: 'Spot Quote Failed',
            description: axiosError.message
          });
          return;
        }

        notification.error({
          message: 'Spot Quote Failed',
          description: axiosError.response.data.SystemMessage
        });
      }
    });
  };

  private renderCurrencyFields(): React.ReactNode {
    const {
      form: { getFieldDecorator }
    } = this.props;
    return (
      <>
        <Row gutter={24}>
          <Col xs={24} lg={12}>
            <SelectField label="From Currency" required options={CurrencyCodes} getFieldDecorator={getFieldDecorator} />
          </Col>
          <Col xs={24} lg={12}>
            <SelectField label="To Currency" required options={CurrencyCodes} getFieldDecorator={getFieldDecorator} />
          </Col>
        </Row>

        <Row>
          <InputField label="Amount" prefix="$" placeholder="640000" required getFieldDecorator={getFieldDecorator} />
        </Row>
      </>
    );
  }

  private renderUserDetails(): React.ReactNode {
    const {
      form: { getFieldDecorator }
    } = this.props;

    return (
      <>
        <Row gutter={24}>
          <Col xs={24} lg={12}>
            <InputField label="First Name" placeholder="John" required getFieldDecorator={getFieldDecorator} />
          </Col>
          <Col xs={24} lg={12}>
            <InputField label="Last Name" placeholder="Doe" required getFieldDecorator={getFieldDecorator} />
          </Col>
        </Row>
        <Row>
          <InputField label="Email" placeholder="example@ofx.com.au" required getFieldDecorator={getFieldDecorator} />
        </Row>

        <Row>
          <InputField
            label="Phone Number"
            prefix="+61"
            placeholder="444 888 444"
            getFieldDecorator={getFieldDecorator}
          />
        </Row>
      </>
    );
  }

  public render() {
    const { isLoadingData } = this.props;

    return (
      <Spin tip={this.renderSpinnerText()} spinning={isLoadingData} indicator={this.renderLoadingIndicator()}>
        <Form onSubmit={this.onSubmit}>
          {this.renderUserDetails()}
          {this.renderCurrencyFields()}
          <Button type="submit">GET A QUOTE</Button>
        </Form>
      </Spin>
    );
  }
}
