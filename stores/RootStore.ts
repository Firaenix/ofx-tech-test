import axios from 'axios';
import { action, observable } from 'mobx';

import { OFXSpotRateResponse } from '../models/OFXSpotRateResponse';

class Store {
  @observable
  public apiResponse?: OFXSpotRateResponse = undefined;

  @observable
  public isFetchingAPI: boolean = false;

  @observable
  public hasError?: boolean = false;

  @action
  private setApiResponse = (response: OFXSpotRateResponse) => {
    this.apiResponse = response;
  };

  @action
  private setIsFetchingAPI = (value: boolean) => {
    this.isFetchingAPI = value;
  };

  @action
  private setHasError = (value: boolean) => {
    this.hasError = value;
  };

  public fetchAPIResponse = async (to: string, from: string, amount: number) => {
    const apiUrl = `https://api.ofx.com/PublicSite.ApiService/OFX/spotrate/Individual/${to}/${from}/${amount}?format=json`;
    try {
      this.setIsFetchingAPI(true);
      const response = await axios.get<OFXSpotRateResponse>(apiUrl);

      if (response.status >= 400) {
        this.setApiResponse(response.data);
        throw new Error(response.data.SystemMessage);
      }

      this.setApiResponse(response.data);
      this.setIsFetchingAPI(false);
    } catch (error) {
      this.setIsFetchingAPI(false);
      this.setHasError(true);
      console.log('Something went wrong fetching spot rates', error);

      // Throw the error up to the caller so they can handle it how theyd like
      throw error;
    }
  };
}

export const RootStore = new Store();

export type Stores = { store: Store };
