import { Form, Input } from 'antd';
import { GetFieldDecoratorOptions } from 'antd/lib/form/Form';
import React from 'react';

export type FieldDecoratorFunc = <T extends Object = {}>(
  id: keyof T,
  options?: GetFieldDecoratorOptions | undefined
) => (node: React.ReactNode) => React.ReactNode;

interface Props {
  label: string;
  placeholder: string;

  required?: boolean;

  // From Ant Design form validator
  getFieldDecorator: FieldDecoratorFunc;

  prefix?: string;
}

export const InputField = (props: Props) => {
  return (
    <Form.Item label={props.label} required={props.required}>
      {props.getFieldDecorator(props.label, {
        rules: [
          {
            required: props.required,
            message: 'This field is required.'
          }
        ]
      })(<Input addonBefore={props.prefix} placeholder={props.placeholder} />)}
    </Form.Item>
  );
};
