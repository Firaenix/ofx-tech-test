import { Form, Select } from 'antd';
import React from 'react';

import { FieldDecoratorFunc } from './InputField';

const { Option } = Select;

interface Props {
  label: string;

  required?: boolean;

  options: string[];

  // From Ant Design form validator
  getFieldDecorator: FieldDecoratorFunc;
}

/**
 * Need this to randomly generate a key for lists in React - just incase items arent uniquely Identified
 */
function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    const r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export const SelectField = (props: Props) => {
  return (
    <Form.Item label={props.label} required={props.required}>
      {props.getFieldDecorator(props.label, {
        rules: [
          {
            required: props.required,
            message: 'This field is required.'
          }
        ],
        initialValue: props.options[0]
      })(
        <Select>
          {props.options.map(x => (
            <Option key={uuid()} value={x}>
              {x}
            </Option>
          ))}
        </Select>
      )}
    </Form.Item>
  );
};
