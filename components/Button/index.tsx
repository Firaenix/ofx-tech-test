import classNames from 'classnames';
import { ReactNode } from 'react';
import React from 'react';

interface Props extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  children: ReactNode;
}

export const Button = (props: Props) => {
  const compileBtnClassNames = () => classNames('ofx-btn');

  return (
    <button className={compileBtnClassNames()} onClick={props.onClick} type={props.type}>
      {props.children}
    </button>
  );
};
