import classNames from 'classnames';
import React from 'react';

export const PageHeader = () => {
  return (
    <div className={classNames('ofx-page-header')}>
      <a className={classNames('ofx-page-header__text')} href="/">
        OFX Quick Quote
      </a>
    </div>
  );
};
